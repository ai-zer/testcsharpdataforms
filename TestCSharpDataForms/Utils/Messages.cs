﻿
using System.ComponentModel;

namespace TestCSharpDataForms.Utils
{
    /// <summary>
    /// Сообщения об ошибках
    /// </summary>
    public enum ErrorMessage
    {
        ErrorReadingAppSettings,
        ContextNotInitialized,
        ErrorFillingDb,
        ErrorCreatingDb,
        ErrorReadingFile,
        ErrorString,
        UnknownErrorString
    }

    /// <summary>
    /// Сообщения пользователю
    /// </summary>
    public enum UserMessages
    {
        ChooseTextFileToCreateDb,
        FileDialogFilterTxtFile,
        MBoxWordsLoaded,
        MBoxWordsLoadedWindowName,
        MBoxDbDelete,
        MBoxDbDeleteWindowName,
        MBoxDbCreate,
        MBoxDbCreateWindowName,
        MBoxDbNotFound,
        MBoxDbNotFoundWindowName,
        MBoxDbFill,
        MBoxDbFillWindowName
    }

    /// <summary>
    /// Все строковые сообщения в приложении находятся здесь.
    /// </summary>
    public class Messages
    {
        public static string GetValue(UserMessages createStatus)
        {
            switch (createStatus)
            {
                case UserMessages.ChooseTextFileToCreateDb:
                    return "Выберите словарь для создания базы данных.";
                case UserMessages.FileDialogFilterTxtFile:
                    return "Текстовый файл|*.txt";
                case UserMessages.MBoxWordsLoaded:
                    return " слов загружено в базу данных";
                case UserMessages.MBoxWordsLoadedWindowName:
                    return "Загрузка завершена";
                case UserMessages.MBoxDbCreate:
                    return "Создан новый файл базы данных";
                case UserMessages.MBoxDbCreateWindowName:
                    return "База данных создана";
                case UserMessages.MBoxDbDelete:
                    return "Обнаружен существующий файл базы данных. Удалить его?";
                case UserMessages.MBoxDbDeleteWindowName:
                    return "Удаление старой базы";
                case UserMessages.MBoxDbNotFound:
                    return "Не обнаружен существующий файл базы данных. Создать его?";
                case UserMessages.MBoxDbNotFoundWindowName:
                    return "Создание новой базы";
                case UserMessages.MBoxDbFill:
                    return "Занести в базу данных словарь?";
                case UserMessages.MBoxDbFillWindowName:
                    return "Заполнение новой базы";
                default:
                    throw new InvalidEnumArgumentException(string.Format("UserMessage string not found for {0}", createStatus));
            }
        }

        public static string GetValue(ErrorMessage createStatus)
        {
            switch (createStatus)
            {
                case ErrorMessage.ErrorString:
                    return "Ошибка.";
                case ErrorMessage.ErrorReadingAppSettings:
                    return "Ошибка считывания настроек.";
                case ErrorMessage.ContextNotInitialized:
                    return "Ошибка инициализации контекста данных.";
                case ErrorMessage.ErrorFillingDb:
                    return "Ошибка чтения файла или внесения данных.";
                case ErrorMessage.ErrorReadingFile:
                    return "Ошибка чтения файла.";
                case ErrorMessage.ErrorCreatingDb:
                    return "Ошибка создания базы данных.";
                case ErrorMessage.UnknownErrorString:
                    return "Произошла неизвестная ошибка.";
                default:
                    throw new InvalidEnumArgumentException(string.Format("UserMessage string not found for {0}", createStatus));

            }
        }
    }
}