﻿
using System;
using System.Collections.Generic;
using System.Linq;
using TestCSharpDataForms.DAL.DAO;
using TestCSharpDataForms.Models;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms.BLL
{
   public class DbOperations
   {
       private WordDao _wordDao = new WordDao();
       
       /// <summary>
       /// Добавляет в БД список слов. Bulk insert
       /// </summary>
       /// <param name="words">Список слов для добавления в базу</param>
       /// <returns>Количество слов, добавленных в базу</returns>
       public int FillDbFromWordList(IEnumerable<string> words )
       {
           List<WordModel> modelsToAdd = words.Select(word => new WordModel {Value = word}).ToList();
           List<WordModel> uniqueModels = _wordDao.RemoveDuplicates(modelsToAdd);
           _wordDao.AddAll(uniqueModels);
           return uniqueModels.Count;
       }

       /// <summary>
       /// Отбирает слова, которые содержат в себе данное подслово
       /// </summary>
       /// <param name="inputText">Подслово, критерий поиска</param>
       /// <returns>Список слов, содержащих введенное подслово</returns>
       public List<WordModel> GetSimilarWords(string inputText)
       {
           try
           {
               var words = _wordDao.Contains(inputText);
               return words.ToList();
           }
           catch (Exception e)
           {
               Logger.Log.Error("Error in DbOperations.GetSimilarWords\r\n" + e.Message, e);
           }
           return null;
       }
       

    }
}
