﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestCSharpDataForms.BLL;
using TestCSharpDataForms.DAL;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms.Forms
{
    public partial class MainForm : Form
    {
        private DbOperations _dbOperations = new DbOperations();

        public MainForm()
        {
            InitializeComponent();
        }

        private void textBoxInput_TextChanged(object sender, EventArgs e)
        {
            FindWords();
        }

        //connect to db
        private void btnLoadDb_Click(object sender, EventArgs e)
        {
            var linesCount = FillDbFromTxt();
            MessageBox.Show(linesCount + Messages.GetValue(UserMessages.MBoxWordsLoaded),
                Messages.GetValue(UserMessages.MBoxWordsLoadedWindowName));
        }

        // create db from text file
        private void btnCreateDb_Click(object sender, EventArgs e)
        {
            CreateDb();
            FillDbConfirmation();
        }

        /// <summary>
        /// Находит подходящие по критерию слова и осуществляет заполнение listbox с результатами. 
        /// </summary>
        private void FindWords()
        {
            listBoxResultLust.Items.Clear();
            if (textBoxInput.Text.Length > 0)
            {
                var words = _dbOperations.GetSimilarWords(textBoxInput.Text);
                if (words != null)
                {
                    foreach (var wordModel in words)
                    {
                        listBoxResultLust.Items.Add(wordModel.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Запрос на открытие текстового файла и занесения его в БД с отображением результата
        /// </summary>
        private void FillDbConfirmation()
        {
            if (
               MessageBox.Show(Messages.GetValue(UserMessages.MBoxDbFill),
                   Messages.GetValue(UserMessages.MBoxDbFillWindowName), MessageBoxButtons.YesNo) ==
               DialogResult.Yes)
            {
                var linesCount = FillDbFromTxt();
                MessageBox.Show(linesCount + Messages.GetValue(UserMessages.MBoxWordsLoaded),
                    Messages.GetValue(UserMessages.MBoxWordsLoadedWindowName));
            }
        }

        /// <summary>
        /// Создает базу, или удаляет и создает если она уже существует.
        /// </summary>
        private void CreateDb()
        {
            try
            {
                DbInitializer di = new DbInitializer();
                if (di.DatabaseIsExist())
                {
                    if (
                        MessageBox.Show(Messages.GetValue(UserMessages.MBoxDbDelete),
                            Messages.GetValue(UserMessages.MBoxDbDeleteWindowName), MessageBoxButtons.YesNo) ==
                        DialogResult.Yes)
                    {
                        di.DeleteDatabase();
                    }
                    else
                    {
                        return;
                    }
                }
                di.CreateDatabase();
                di.InitializeDatabase();
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Error in MainForm.CreateDb\r\n" + ex.Message, ex);
                MessageBox.Show(Messages.GetValue(ErrorMessage.ErrorCreatingDb), Messages.GetValue(ErrorMessage.ErrorString));
            }
        }
       

        /// <summary>
        /// Заполняет базу данных словами из текстового файла
        /// </summary>
        /// <returns>Количество загруженных слов</returns>
        private int FillDbFromTxt()
        {
            try
            {
                OpenFileDialog openTextFileDialog = new OpenFileDialog();
                openTextFileDialog.Filter = Messages.GetValue(UserMessages.FileDialogFilterTxtFile);
                openTextFileDialog.Title = Messages.GetValue(UserMessages.ChooseTextFileToCreateDb);
                if (openTextFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamReader sr = new StreamReader(openTextFileDialog.FileName, true);
                    Encoding encoding = sr.CurrentEncoding;
                    sr.Close();

                    var lines = File.ReadLines(openTextFileDialog.FileName, encoding);
                    return _dbOperations.FillDbFromWordList(lines);
                }
            }
            catch (IOException ioex)
            {
                Logger.Log.Error("Error in MainForm.FillDb\r\n" + ioex.Message, ioex);
                MessageBox.Show(Messages.GetValue(ErrorMessage.ErrorReadingFile), Messages.GetValue(ErrorMessage.ErrorString));
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Error in MainForm.FillDb\r\n" + ex.Message, ex);
                MessageBox.Show(Messages.GetValue(ErrorMessage.ErrorFillingDb), Messages.GetValue(ErrorMessage.ErrorString));
            }
            return 0;
        }
    }
}