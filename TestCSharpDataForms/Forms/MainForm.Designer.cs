﻿namespace TestCSharpDataForms.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxInput = new System.Windows.Forms.TextBox();
            this.listBoxResultLust = new System.Windows.Forms.ListBox();
            this.btnLoadDb = new System.Windows.Forms.Button();
            this.btnCreateDb = new System.Windows.Forms.Button();
            this.progressBarLoadIndicator = new System.Windows.Forms.ProgressBar();
            this.panelProgressIndicator = new System.Windows.Forms.Panel();
            this.labelCurrentOperation = new System.Windows.Forms.Label();
            this.labelStaticLabel = new System.Windows.Forms.Label();
            this.panelProgressIndicator.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxInput
            // 
            this.textBoxInput.Location = new System.Drawing.Point(13, 13);
            this.textBoxInput.Name = "textBoxInput";
            this.textBoxInput.Size = new System.Drawing.Size(359, 20);
            this.textBoxInput.TabIndex = 0;
            this.textBoxInput.TextChanged += new System.EventHandler(this.textBoxInput_TextChanged);
            // 
            // listBoxResultLust
            // 
            this.listBoxResultLust.FormattingEnabled = true;
            this.listBoxResultLust.Location = new System.Drawing.Point(13, 40);
            this.listBoxResultLust.Name = "listBoxResultLust";
            this.listBoxResultLust.Size = new System.Drawing.Size(359, 173);
            this.listBoxResultLust.TabIndex = 1;
            // 
            // btnLoadDb
            // 
            this.btnLoadDb.Location = new System.Drawing.Point(12, 219);
            this.btnLoadDb.Name = "btnLoadDb";
            this.btnLoadDb.Size = new System.Drawing.Size(150, 25);
            this.btnLoadDb.TabIndex = 2;
            this.btnLoadDb.Text = "Загрузить базу данных";
            this.btnLoadDb.UseVisualStyleBackColor = true;
            this.btnLoadDb.Click += new System.EventHandler(this.btnLoadDb_Click);
            // 
            // btnCreateDb
            // 
            this.btnCreateDb.Location = new System.Drawing.Point(222, 219);
            this.btnCreateDb.Name = "btnCreateDb";
            this.btnCreateDb.Size = new System.Drawing.Size(150, 25);
            this.btnCreateDb.TabIndex = 3;
            this.btnCreateDb.Text = "Создать базу данных";
            this.btnCreateDb.UseVisualStyleBackColor = true;
            this.btnCreateDb.Click += new System.EventHandler(this.btnCreateDb_Click);
            // 
            // progressBarLoadIndicator
            // 
            this.progressBarLoadIndicator.Location = new System.Drawing.Point(20, 159);
            this.progressBarLoadIndicator.Name = "progressBarLoadIndicator";
            this.progressBarLoadIndicator.Size = new System.Drawing.Size(325, 23);
            this.progressBarLoadIndicator.TabIndex = 4;
            // 
            // panelProgressIndicator
            // 
            this.panelProgressIndicator.Controls.Add(this.labelCurrentOperation);
            this.panelProgressIndicator.Controls.Add(this.labelStaticLabel);
            this.panelProgressIndicator.Controls.Add(this.progressBarLoadIndicator);
            this.panelProgressIndicator.Location = new System.Drawing.Point(13, 13);
            this.panelProgressIndicator.Name = "panelProgressIndicator";
            this.panelProgressIndicator.Size = new System.Drawing.Size(359, 200);
            this.panelProgressIndicator.TabIndex = 5;
            this.panelProgressIndicator.Visible = false;
            // 
            // labelCurrentOperation
            // 
            this.labelCurrentOperation.AutoSize = true;
            this.labelCurrentOperation.Location = new System.Drawing.Point(20, 44);
            this.labelCurrentOperation.Name = "labelCurrentOperation";
            this.labelCurrentOperation.Size = new System.Drawing.Size(0, 13);
            this.labelCurrentOperation.TabIndex = 6;
            // 
            // labelStaticLabel
            // 
            this.labelStaticLabel.AutoSize = true;
            this.labelStaticLabel.Location = new System.Drawing.Point(17, 27);
            this.labelStaticLabel.Name = "labelStaticLabel";
            this.labelStaticLabel.Size = new System.Drawing.Size(135, 13);
            this.labelStaticLabel.TabIndex = 5;
            this.labelStaticLabel.Text = "Выполняется операция...";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.btnCreateDb);
            this.Controls.Add(this.btnLoadDb);
            this.Controls.Add(this.listBoxResultLust);
            this.Controls.Add(this.textBoxInput);
            this.Controls.Add(this.panelProgressIndicator);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(400, 300);
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "MainForm";
            this.Text = "Test C# Data. Windows Forms";
            this.panelProgressIndicator.ResumeLayout(false);
            this.panelProgressIndicator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxInput;
        private System.Windows.Forms.ListBox listBoxResultLust;
        private System.Windows.Forms.Button btnLoadDb;
        private System.Windows.Forms.Button btnCreateDb;
        private System.Windows.Forms.ProgressBar progressBarLoadIndicator;
        private System.Windows.Forms.Panel panelProgressIndicator;
        private System.Windows.Forms.Label labelCurrentOperation;
        private System.Windows.Forms.Label labelStaticLabel;
    }
}

