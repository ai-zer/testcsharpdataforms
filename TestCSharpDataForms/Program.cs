﻿using System;
using System.IO;
using System.Windows.Forms;
using TestCSharpDataForms.Forms;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Logger.InitLogger();

            if (!Directory.Exists("AppData"))
                Directory.CreateDirectory("AppData");
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");

           AppDomain.CurrentDomain.SetData(
  "DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppData"));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
