﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms.DAL
{
    public class DbInitializer
    {
        private BaseContext _localContext = new BaseContext();
        
        public void InitializeDatabase()
        {
            try
            {
                _localContext.Database.Initialize(true);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in DbInitializer.InitializeDatabase\r\n" + e.Message, e);
            }
        }

        /// <summary>
        /// Проверяет, существует ли на диске и есть ли подключение к базе данных
        /// </summary>
        /// <returns>True, если база данных существует, иначе false</returns>
        public bool DatabaseIsExist()
        {
            return (_localContext.Database.Exists()) ? true : false;
        }

        /// <summary>
        /// Создание пустой базы данных
        /// </summary>
        public void CreateDatabase()
        {
            try
            {
                // Создание базы данных без схемы миграции Entity Framework
                ((IObjectContextAdapter) _localContext).ObjectContext.CreateDatabase();
                //LocalContext.Database.Initialize(true);
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Error in DbInitializer.CreateDatabase\r\n" + ex.Message, ex);
            }
        }

        /// <summary>
        /// Удаление файла базы данных с диска
        /// </summary>
        public void DeleteDatabase()
        {
            try
            {
                //((IObjectContextAdapter) LocalContext).ObjectContext.DeleteDatabase();
                Database.Delete(AppSettings.ConnectionString);
            }
            catch (Exception ex)
            {
                Logger.Log.Error("Error in DbInitializer.DeleteDatabase\r\n" + ex.Message, ex);
            }
        }
    }
}