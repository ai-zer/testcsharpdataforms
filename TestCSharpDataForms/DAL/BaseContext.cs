﻿using System.Data.Entity;
using TestCSharpDataForms.Models;

namespace TestCSharpDataForms.DAL
{
    /// <summary>
    /// Контекст для доступа к базе данных
    /// </summary>
    public class BaseContext : DbContext
    {
        /// <summary>
        /// Коллекция слов
        /// </summary>
        public virtual DbSet<WordModel> Words { get; set; }

        public BaseContext()
            : base(AppSettings.ConnectionName)
        {

        }
    }
}