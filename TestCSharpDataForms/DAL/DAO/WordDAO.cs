﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestCSharpDataForms.Models;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms.DAL.DAO
{
    /// <summary>
    /// Объект доступа к таблице 'Words'
    /// </summary>
    public class WordDao : BaseDao<WordModel>
    {
        public WordDao()
        {
            LocalDbContext.Set<WordModel>();
        }
        
        /// <summary>
        /// Возвращает список слов, которые содержат в себе подстроку
        /// </summary>
        /// <param name="text">Подстрока, критерий отбора</param>
        /// <returns>Коллекция слов</returns>
        public virtual List<WordModel> Contains(string text)
        {
            return LocalDbSet.Where(x => x.Value.Contains(text)).ToList();
        }

        /// <summary>
        /// Находит в базе слово, указанное в параметре
        /// </summary>
        /// <param name="text">Слово, для которого будет произведен поиск дубликата</param>
        /// <returns>Модели, которая уже существует в базе</returns>
        public virtual List<WordModel> GetDuplicates(string text)
        {
            return LocalDbSet.Where(x => x.Value == text).ToList();
        }

        /// <summary>
        /// Удаляет из списка слова, которые уже существуют в базе
        /// </summary>
        /// <param name="criteriaModels">Список, по которому ищутся дубликаты</param>
        /// <returns>Список без дубликатов</returns>
        public virtual List<WordModel> RemoveDuplicates(List<WordModel> criteriaModels)
        {
            try
            {
                return LocalDbSet.SelectMany(x => criteriaModels.Where(c => c.Value == x.Value).ToList()).ToList();
                //var dbset = LocalDbSet.ToList();
                //foreach (var item in dbset)
                //    criteriaModels.RemoveAll(x => x.Value == item.Value);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in WordDao.RemoveDuplicates\r\n" + e.Message, e);
            }
            return criteriaModels;
        }

    }
}