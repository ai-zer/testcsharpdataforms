﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms.DAL.DAO
{
    /// <summary>
    /// Базовый объект Data Access Object. 
    /// </summary>
    /// <typeparam name="T">Модель</typeparam>
    public class BaseDao<T> where T : class
    {
        
        protected BaseContext LocalDbContext { get; set; }

        protected DbSet<T> LocalDbSet { get; set; }

        public BaseDao()
        {
            LocalDbContext = new BaseContext();
            if (LocalDbContext == null)
                throw new ArgumentNullException(Messages.GetValue(ErrorMessage.ContextNotInitialized));
            LocalDbSet = LocalDbContext.Set<T>();
        }


        public List<T> FindAll()
        {
            return LocalDbSet.ToList<T>();
        }
        /// <summary>
        /// Возвращает количество элементов в наборе данных LocalDbSet
        /// </summary>
        /// <returns>Количество всех элементов в LocalDbSet</returns>
        public int Count()
        {
            return LocalDbSet.Count();
        }

        public virtual T FindById(int id)
        {
            try
            {
                return LocalDbSet.Find(id);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in BaseDao.FindById\r\n" + e.Message, e);
                return null;
            }
        }

        public virtual void Add(T entity)
        {
            try
            {
                LocalDbSet.Add(entity);
                LocalDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in BaseDao.Add\r\n" + e.Message, e);
            }
        }

        public virtual void AddAll(List<T> entities)
        {
            try
            {
                LocalDbSet.AddRange(entities);
                LocalDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in BaseDao.AddAll\r\n" + e.Message, e);
            }
        }

        public virtual void AddAll(IEnumerable<T> entities)
        {
            AddAll(entities.ToList());
        }

        public virtual void Update(T entity)
        {
            try
            {
                //3. Mark entity as modified
                LocalDbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                LocalDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in BaseDao.Update\r\n" + e.Message, e);
            }
        }

        public virtual void Delete(int id)
        {
            try
            {
                T entity = FindById(id);
                Delete(entity);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in BaseDao.Delete\r\n" + e.Message, e);
            }
        }

        public virtual void Delete(T entity)
        {
            try
            {
                LocalDbSet.Remove(entity);
                LocalDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Error in BaseDao.Delete\r\n" + e.Message, e);
            }
        }

    }
}