﻿using System.Configuration;
using TestCSharpDataForms.Utils;

namespace TestCSharpDataForms
{
    public class AppSettings
    {
        /// <summary>
        /// Название строки подключения
        /// </summary>
        public static string ConnectionName
        {
            get { return ReadSetting("ConnectionName"); }
        }

        /// <summary>
        /// Строка подключения
        /// </summary>
        public static string ConnectionString
        {
            get { return ReadSetting("ConnectionString"); }
        }

        /// <summary>
        /// Считывает значения по ключу из файла app.config
        /// </summary>
        /// <param name="key">Ключ, название элемента</param>
        /// <returns>Значение элемента</returns>
        private static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                return appSettings[key] ?? string.Empty;
            }
            catch (ConfigurationErrorsException e)
            {
                Logger.Log.Error("Error in AppSettings.ReadSettings\r\n" + Messages.GetValue(ErrorMessage.ErrorReadingAppSettings), e);
            }                
            return string.Empty;
        }


    }


}