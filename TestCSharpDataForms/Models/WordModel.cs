﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestCSharpDataForms.Models
{
    /// <summary>
    /// Модель "слово"
    /// </summary>
    [Table("Words")]
   public class WordModel
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        public string Value { get; set; }

    
    }
}
